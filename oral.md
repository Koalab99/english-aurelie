# Diabetic Retinopathy
## Intro 
We ~~are going to~~ **will** present the diabetic retinopathy’s disease. It **is the** result of diabetes complications ~~which it is not balance~~ **What do you mean?**. In fact, the excess of sugar ~~damage blood vessels of the retina~~ **damages the retina's blood vessels** and impact the vision.  There are two ~~types~~ **stages** : the early diabetic retinopathy and the advanced diabetic retinopathy.
There are two types of diabetes, type 1 and type 2. The type 1 is an insulin dependent diabete, it is called the ~~skinny~~ **lean or thin** diabete. The type 2 **diabetes** is no**t** insulin dependent ~~diabete~~ and it is called the fat diabete. This disease is generally present ~~of the~~ on both eye**s**.
In this presentation, we are going to ~~speak~~ **talk** in the first ~~time~~ **place** about symptoms and causes. ~~After~~ **Then**, we **will** explain the diagnosis and the ophthalmologic treatment for the disease. ~~To finish~~ **In closing**, we **will** ~~speak~~ **talk** about the orthoptis**ts**’~~t~~ role in **the treatment of** this disease.

## Causes
When the blood vessels are damage**d** by the sugar’s excess, the blood vessels are not nourrish**ed** properly and they ~~became tinies and damages~~ **become tiny and damaged**. As a result, the eye develop new blood vessels in the retina but these new blood vessels don’t develop properly.
There are two types of diabetic retinopathy:
The Early diabetic retinopathy is a non proliferative diabetic retinopathy (NPDR) in this case, new blood vessels aren't growing and proliferating.
Advanced diabetic retinopathy is a proliferative diabetic retinopathy. The new blood vessels can leak and ~~blood~~ **bleed** in the vitreous and damage vision.

~~The causes of diabete retinopathy are large. There are :~~

**There is many factors that can cause diabetes retinopathy, the most relevants are :**
-	~~Duration of diabetes~~ **Diabetes' duration ?**
-	Poor control of your blood sugar level 
-	High blood pressure
-	High cholesterol
-	Pregnancy ~~when you wait **for** a child~~ *(pregnancy = when you wait for a child : enceinte quand tu attends un enfant)*
-	~~Tobacco use~~ **Smoking** 
-	Being African-American, Hispanic or Native American

## Complications and risks
We can have complications with diabetic retinopathy like a vitreous hemorragy, this bring**s** ~~to~~ spots in the vision. We can **also** have ~~also~~ a retinal detachment, glaucome and blindness.
(To avoid ~~this~~ **the** risks and complications of this disease, it’s advise**d** to manage the diabete, keep~~ing~~ the ~~blodd~~ **blood** pressure and the cholesterol under ~~the~~ control and pay attention to vision changes.)

## Diagnosis
For a good diagnosis, the doctor must make fundus of the eye with eye drops which dilate pupils. The ophthalmologist ~~researchs~~ **look for**: abnormal blood vessels, scar tissue (=tissue cicatriciel) , retinal detachment, abnormalities in the optic nerve ~~and~~ **,** bleeding in the vitreous and a diabetic macular edema.

## Treatment (Cloé)
## Orthoptist’s role
The orthoptist sees the patient before **he sees** the doctor and ~~realizing~~ **perform** different exams which will be analyze**d** by the doctor. She realizes the screening (dépistage) and the assess (evaluation) of this disease.
To begin, she asks the patient about the symptoms, the treatment that he is taking and the rate~~s~~ of **glicated/glycosylated** hemoglobin. ~~gliquée. ( je chercherai la traduction)~~
She questions about medical history and general background.
She assess~~s~~ the near vision and the far vision. She measures eye pressure and makes some complementary examination. To complet**e the** exam, she realizes an OCT and a picture of the retina with measuring instruments “optos” and "eidon”.
If the patient lies ( se situe) in low vision, she can help the patient by exercising **his eyes** and ~~withoptic helps to improve~~ **by improving** their daily life **with optic helps**.
